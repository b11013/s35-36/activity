const express = require("express");

const router = express.Router(); //allows access to HTTP Methods

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

//create task route//lastpart
router.post("/", taskControllers.createTaskControllers);

router.get("/", taskControllers.getAllTasksController);

router.get("/getSingleTask/:id", taskControllers.getSingleTaskController);

router.put("/updateTask/:id", taskControllers.updateTaskController);

module.exports = router;