const express = require("express");

const router = express.Router();

const userControllers = require("../controllers/userControllers");

router.post("/", userControllers.createUserControllers);

router.get("/", userControllers.getAllUsersController);

router.get("/getSingleUser/:id", userControllers.getSingleUsernameController);

router.put("/:id", userControllers.updateUsernameController);

module.exports = router;