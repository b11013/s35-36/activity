//import the Task model in our controllers to get access to Task Model
const Task = require("../models/Task");

//create Task

module.exports.createTaskControllers = (req, res) => {
    console.log(req.body); //checking captured data from request body

    Task.findOne({ name: req.body.name })
        .then((result) => {
            //promise - to become asynchronous (can run along with other process)

            console.log(result);
            //expected output:
            /*{ 
    _objectID:04sasa1saa5s2,
    name: NameofTask,
    status: "pending" 
}*/
            if (result !== null && result.name === req.body.name) {
                return res.send("Duplicate task found");
            } else {
                let newTask = new Task({
                    name: req.body.name,
                    status: req.body.status,
                });
                newTask
                    .save()
                    .then((result) => res.send(result))
                    .catch((error) => res.send(error));
            }
        })
        .catch((error) => res.send(error));
};

//TO RETRIEVE ALL TASKS - GET

module.exports.getAllTasksController = (req, res) => {
    Task.find({})
        //can set criteria inside {} ^
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//single task retrieval
module.exports.getSingleTaskController = (req, res) => {
    console.log(req.params);
    //findbyid mongoose method
    Task.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//updating single task status via ID
module.exports.updateTaskController = (req, res) => {
    console.log(req.param.id);
    console.log(req.body);

    //will hold what property we like to update
    let updates = {
        status: req.body.status,
    };
    // findByIdAndUpdate - has 3 arguments
    // 1- where to get the id (req.params.id)
    // 2- what is the update (updates variable)
    // 3- optional: {new: true} - return the updated version of the doc we are updating
    Task.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedTask) => res.send(updatedTask))
        .catch((err) => res.send(err));
};