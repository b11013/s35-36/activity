const User = require("../models/User");

//POST USERS
module.exports.createUserControllers = (req, res) => {
    User.findOne({ username: req.body.username })
        .then((result) => {
            if (result !== null && result.username === req.body.username) {
                return res.send("Username already exists!");
            } else {
                let newUser = new User({
                    username: req.body.username,
                    password: req.body.password,
                });
                newUser
                    .save()
                    .then((result) => res.send(result))
                    .catch((error) => res.send(error));
            }
        })
        .catch((error) => res.send(error));
};

//GET USERS
module.exports.getAllUsersController = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//GET SINGLE USERNAME
module.exports.getSingleUsernameController = (req, res) => {
    User.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//UPDATE SINGLE USERNAME VIA ID
module.exports.updateUsernameController = (req, res) => {
    let updates = {
        username: req.body.username,
    };

    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedTask) => res.send(updatedTask))
        .catch((err) => res.send(err));
};