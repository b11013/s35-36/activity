// basic express server

const express = require("express"); //required modules
const mongoose = require("mongoose");

const port = 4000; //port

const app = express(); //server

mongoose.connect(
    "mongodb+srv://admin:admin@wdc028-course-booking.sbutb.mongodb.net/tasks182?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
); //mongoose connection

let db = mongoose.connection; //will create a notif if db conn is success or not

db.on("error", console.error.bind(console, "DB Connection Error")); //error handler
//console.error.bind will show error in terminal and in browser

db.once("open", () => console.log("Successfully connected to MongoDB")); //once success

app.use(express.json()); //middleware
app.use(express.urlencoded({ extended: true })); //for reading data forms

//grouping the routes

const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));