//this file is a model

const mongoose = require("mongoose"); //call mongoose cuz it has schema method

//schema is the blueprint for the document

//const { default: mongoose } = require("mongoose");

const taskSchema = new mongoose.Schema({
    name: String,
    status: String,
});

//mongoose model (partner of schema)

/* mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>) */

module.exports = mongoose.model("Task", taskSchema);
//modules.exports - to use outside other module
//.model() - used in manipulating data